# -*- coding: utf-8 -*-
import unittest
import os

from wiki_core import *


def create_expected_full_file_name(text_selected, expected_file_name):
    """ Create the complete path for the file as a string. """
    link_to_format = '[{}]({} "{}")'
    file_name = '{}/{}.md'.format(HOME_WIKI_DIR, expected_file_name)
    expected = link_to_format.format(text_selected, file_name,
                                     text_selected)
    return expected


class WikiStringTest(unittest.TestCase):

    def test_wiki_replace(self):
        text_selected = ' My new wiki file '
        expected = create_expected_full_file_name(text_selected, 'my_new_wiki_file')
        self.assertEquals(expected, get_markdown_link(text_selected))

        text_selected = '08/08/17'
        expected = create_expected_full_file_name(text_selected, '08_08_17')
        self.assertEquals(expected, get_markdown_link(text_selected))

    def test_wiki_ignore_empty_strings_in_link(self):
        self.assertEquals('', get_markdown_link(''))
        self.assertEquals('', full_file_name(''))


class WikiEditTest(unittest.TestCase):

    def setUp(self):
        self.wiki_page_file = '{}/wiki/my_page.md'.format(HOME_DIR)
        self.link_name = 'My page'

    def test_edit_wiki_page(self):
        content = 'This is the wiki page link [{}]({} "{}") I want to edit'
        content = content.format(self.link_name, self.wiki_page_file,
                                 self.link_name)

        result = extract_wiki_page_paths(content)

        self.assertEquals([self.wiki_page_file], result)

    def test_edit_unique_wiki_separed_by_comma(self):
        content = 'First page [{0}]({1} "{2}"), second page [{0}]({1} "{2}").'
        content = content.format(self.link_name, self.wiki_page_file,
                                 self.link_name)

        result = extract_wiki_page_paths(content)

        self.assertEquals([self.wiki_page_file], result)
