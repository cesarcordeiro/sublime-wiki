#!/bin/bash
# Will convert markdown wiki files into html.

command -v markdown_py > /dev/null 2>&1 || { echo >&2 "Python markdown is required to convert wiki."; exit 1; }

function replace_pattern_in_file
{
	PATTERN_TO_BE_FOUND="$1"
	NEW_PATTERN="$2"
	THE_FILE="$3"

	# Using delimiter '#' to avoid conflict with patterns containing directories.
	# Note: this only works with GNU sed.
	sed -i "s#$PATTERN_TO_BE_FOUND#$NEW_PATTERN#g" "$THE_FILE"
}

WIKI_FOLDER="$HOME/wiki"
HTML_FOLDER="$HOME/wiki_html"

# If the HTML folder doesn't exist, make one.
if [ ! -d "$HTML_FOLDER" ]
then
	mkdir "$HTML_FOLDER"
fi

for filename in "$WIKI_FOLDER/"*.md
do
	HTML_NAME="$(basename $filename)".html
	HTML_FILE="$HTML_FOLDER/$HTML_NAME"

	markdown_py "$filename" > "$HTML_FILE"

	replace_pattern_in_file "$WIKI_FOLDER" "$HTML_FOLDER" "$HTML_FILE"
	replace_pattern_in_file ".md" ".md.html" "$HTML_FILE"
done