# -*- coding: utf-8 -*-
import sublime
import sublime_plugin
import os

from .wiki_core import *


HOME_FILE = '{}/{}'.format(HOME_WIKI_DIR, '/index.md')


def init_home():
    if not os.path.exists(HOME_WIKI_DIR):
        sublime.error_message('Please create a wiki directory.')

    if not os.path.isfile(HOME_FILE):
        # Touch file
        open(HOME_FILE, 'a').close()


def is_markdown_file(view):
    return 'Markdown' in view.settings().get('syntax')


class OpenWikiCommand(sublime_plugin.WindowCommand):
    """
    Base command to open index.md file.
    """

    def run(self):
        current_view = self.window.active_view()
        if not is_markdown_file(current_view):
            init_home()
            self.window.open_file(HOME_FILE)


class DoWikiLinkCommand(sublime_plugin.TextCommand):
    """
    Command to transform selected text to new wiki file.
    """

    def run(self, edit):
        if is_markdown_file(self.view):
            current_selected = self.view.sel()[0]
            selected_str = self.view.substr(current_selected)
            self.__open_file_in_editor(selected_str)
            link = get_markdown_link(selected_str)
            self.view.replace(edit, current_selected, link)

    def __open_file_in_editor(self, selected_text):
        file_name = full_file_name(selected_text)
        if full_file_name:
            window = self.view.window()
            window.open_file(file_name)
            window.focus_view(self.view)


class EditWikiLinkCommand(sublime_plugin.TextCommand):
    """
    Command to edit pages from links in pages.
    """

    def run(self, edit):
        current_line = self.__select_current_line()
        pages_to_edit = extract_wiki_page_paths(current_line)
        window = self.view.window()

        for page in pages_to_edit:
            window.open_file(page)
            window.focus_view(self.view)

    def __select_current_line(self):
        return self.view.substr(self.view.line(self.view.sel()[0]))
