# -*- coding: utf-8 -*-
import os
import re

HOME_DIR = os.path.expanduser('~')
HOME_WIKI_DIR = "{}/wiki".format(HOME_DIR)


def _has_text(text):
    """ Check if string has content. """
    return len(text) > 0


def file_name_from_link(selected):
    """ Transform string to a markdown filename. """
    file_name = selected.strip()

    chars_to_replace = [' ', '/']
    for char in chars_to_replace:
        file_name = file_name.replace(char, '_')

    file_name = file_name.lower()
    return '{}.{}'.format(file_name, 'md')


def full_file_name(selected_text):
    """ Transform string to a full path filename. """
    if not _has_text(selected_text):
        return ''

    file_name = file_name_from_link(selected_text)
    full_path = '{}/{}'.format(HOME_WIKI_DIR, file_name)
    return full_path


def get_markdown_link(selected):
    """ Transform string to markdown link. """
    if not _has_text(selected):
        return ''

    file_name = full_file_name(selected)
    return '[{}]({} "{}")'.format(selected, file_name, selected)


def extract_wiki_page_paths(line_content):
    """ Extract markdown links from given string. """
    pattern = HOME_WIKI_DIR + '.+?(?=\s)'
    result = re.findall(pattern, line_content)
    return list(set(result))
